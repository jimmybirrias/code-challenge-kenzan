package org.bitbucket.kenzan.entities;

public enum Status {
	ACTIVE("active"),
	INACTIVE("inactive");
	
	private String text;
	
	Status (String text) {
		this.text = text;
	}
	
	public String getStatus() {
		return this.text;
	}
	

}
