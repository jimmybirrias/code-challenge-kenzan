package org.bitbucket.kenzan.entities;

import java.util.List;

import org.json.JSONObject;

public interface IEmployee {
	
	public List<Employee> findAllEmployees();
	
	public boolean createUser(JSONObject employee);
	
	public boolean deleteUser(String id);
	
	public String generateId(String name, String date);
	
	public Employee getEmployeeByID(String id);
	
	public boolean updateUserById(String id, JSONObject body);

}
