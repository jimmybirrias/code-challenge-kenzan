package org.bitbucket.kenzan.entities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class Employee {
	private String id;
	private String firstName;
	private String middleName;
	private String lastName;
	private Date dateBirth, dateEmployment;
	private Status status;
	
	public Employee(String id, String firstName, String middleName, String lastName, Date dateBirth,
			Date dateEmployment, Status status) {
		this.id = id;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.dateBirth = dateBirth;
		this.dateEmployment = dateEmployment;
		this.status = status;
	}
	
	public Employee(JSONObject json) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		this.id = json.getString(EmployeeJSONKeys.ID);
		this.firstName = json.getString(EmployeeJSONKeys.FIRSTNAME);
		this.middleName = json.getString(EmployeeJSONKeys.MIDDLENAME);
		this.lastName = json.getString(EmployeeJSONKeys.LASTNAME);
		try {
			this.dateBirth = format.parse(json.getString(EmployeeJSONKeys.DATEBIRTH));
			this.dateEmployment = format.parse(json.getString(EmployeeJSONKeys.DATEEMPLOYMENT));
		} catch (JSONException | ParseException e) {
			System.out.println("The date can not be formated as yyyy-MM-dd");
		}
		this.status = json.getString("status").equalsIgnoreCase("active")?Status.ACTIVE:Status.INACTIVE;
	}
	
	public Employee() {
	}
	
	public Employee(Employee e) {
		try {
			this.id = e.getId();
			this.firstName = e.getFirstName();
			this.middleName = e.getMiddleName();
			this.lastName = e.getLastName();
			this.dateBirth = e.getDateBirth();
			this.dateEmployment = e.getDateEmployment();
			this.status = e.getStatus();
		} catch (Exception e1) {
			System.out.println("The date can not be formated as yyyy-MM-dd");
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(Date dateBirth) {
		this.dateBirth = dateBirth;
	}

	public Date getDateEmployment() {
		return dateEmployment;
	}

	public void setDateEmployment(Date dateEmployment) {
		this.dateEmployment = dateEmployment;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		try {
			return ow.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return null;
		}
	}

}
