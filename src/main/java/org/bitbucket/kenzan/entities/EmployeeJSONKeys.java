package org.bitbucket.kenzan.entities;

public class EmployeeJSONKeys {
	public static String FIRSTNAME = "firstName";
	public static String LASTNAME = "lastName";
	public static String MIDDLENAME = "middleName";
	public static String DATEBIRTH = "dateBirth";
	public static String DATEEMPLOYMENT = "dateEmployment";
	public static String STATUS = "status";
	public static String ID = "id";
}
