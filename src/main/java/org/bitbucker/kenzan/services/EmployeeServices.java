package org.bitbucker.kenzan.services;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bitbucker.kenzan.App;
import org.bitbucket.kenzan.entities.Employee;
import org.bitbucket.kenzan.entities.EmployeeJSONKeys;
import org.bitbucket.kenzan.entities.IEmployee;
import org.bitbucket.kenzan.entities.Status;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class EmployeeServices implements IEmployee {
	private static EmployeeServices INSTANCE = null;

	private EmployeeServices() {
		/* Create a empty constructor to allow the singleton instance */ 
	}

	public static EmployeeServices getEmployeeServiceIntance() {
		/* Allow just create one instance of EmployeeService, I have used Singleton because
		 	1) In the MainServie we only need an EmployeeService instance, this instance can perform all the operations.
		 	2) Using Singleton, we can avoid waste memory creating multiples instance of the same object to perform operations in the same context*/
		if (INSTANCE == null) {
			INSTANCE = new EmployeeServices();
		}
		return INSTANCE;
	}

	public List<Employee> findAllEmployees() {
		List<Employee> list = new ArrayList<>();
		try {
			JSONTokener tokener = new JSONTokener(new FileReader(App.jsonFile));
			JSONObject current = new JSONObject(tokener);
			JSONArray users = current.getJSONArray("employees");

			for (Object object : users) {
				JSONObject user = new JSONObject(object.toString());
				list.add(new Employee(user));
			}
		} catch (FileNotFoundException e) {
			System.out.println("The file can not be found");
			return null;
		}

		list.sort((e1, e2) -> e1.getId().compareTo(e2.getId()));

		List<Employee> filteredList = list.stream()
				.filter(p -> !p.getStatus().getStatus().equalsIgnoreCase(Status.INACTIVE.getStatus()))
				.map(Employee::new).collect(Collectors.toCollection(ArrayList::new));

		return filteredList;
	}

	public List<Employee> getAllEmployees() {
		List<Employee> list = new ArrayList<>();
		try {
			JSONTokener tokener = new JSONTokener(new FileReader(App.jsonFile));
			JSONObject current = new JSONObject(tokener);
			JSONArray users = current.getJSONArray("employees");

			for (Object object : users) {
				JSONObject user = new JSONObject(object.toString());
				list.add(new Employee(user));
			}
		} catch (FileNotFoundException e) {
			System.out.println("The file can not be found");
			return null;
		}

		list.sort((e1, e2) -> e1.getId().compareTo(e2.getId()));

		return list;
	}

	public boolean createUser(JSONObject employee) {
		List<Employee> currentList = getAllEmployees();
		try {
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date dateEmployee = new Date();
			Date dateBirth = format.parse(employee.getString(EmployeeJSONKeys.DATEBIRTH));

			Employee e = new Employee(
					generateId(employee.getString(EmployeeJSONKeys.FIRSTNAME), format.format(dateEmployee)),
					employee.getString(EmployeeJSONKeys.FIRSTNAME), employee.getString(EmployeeJSONKeys.MIDDLENAME),
					employee.getString(EmployeeJSONKeys.LASTNAME), dateBirth, dateEmployee,
					employee.getString(EmployeeJSONKeys.STATUS).equals("active") ? Status.ACTIVE : Status.INACTIVE);
			currentList.add(e);
			saveNewestUser(currentList);

		} catch (ParseException e) {
			return false;
		}
		return true;
	}

	public boolean deleteUser(String id) {
		List<Employee> current = getAllEmployees();
		Employee e = getEmployeeByID(id);

		if (e != null) {
			System.out.println("Trying to delete the following employee ====> " + e);

			List<Employee> newListEmployee = current.stream().filter(p -> !p.getId().equals(id)).map(Employee::new)
					.collect(Collectors.toCollection(ArrayList::new));

			e.setStatus(Status.INACTIVE);
			newListEmployee.add(e);

			System.out.println("List after filter ====> " + newListEmployee);
			saveNewestUser(newListEmployee);

			return true;
		} else {
			System.out.println("The employee with id [ " + id + " ] does not exist");
			return false;
		}
	}

	public String generateId(String name, String date) {
		String hash = name + "-" + date;
		int h = 7;

		for (int i = 0; i < hash.length(); i++) {
			h = h * 31 + hash.charAt(i);
		}

		return String.valueOf(h);
	}

	public void saveNewestUser(List<Employee> currentList) {
		JSONObject current = new JSONObject();
		JSONArray array = new JSONArray();

		currentList.stream().forEach(e -> {
			array.put(e);
		});
		current.put("employees", array);

		try {
			FileWriter fw = new FileWriter(App.jsonFile);
			fw.write(current.toString());
			fw.close();
			System.out.println("The employee list file was updated");
		} catch (IOException e1) {
			System.out.println("The file can not be saved");
		}
	}

	@Override
	public Employee getEmployeeByID(String id) {
		List<Employee> current = getAllEmployees();
		Stream<Employee> filterList = current.stream().filter(
				p -> p.getId().equals(id) && p.getStatus().getStatus().equalsIgnoreCase(Status.ACTIVE.getStatus()));

		try {
			return filterList.findFirst().get();
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	@Override
	public boolean updateUserById(String id, JSONObject body) {
		List<Employee> current = getAllEmployees();
		Employee e = getEmployeeByID(id);

		if (e != null) {
			System.out.println("Trying to update the following employee with ====> " + id);

			List<Employee> newEmployeeList = current.stream().filter(p -> !p.getId().equals(id)).map(Employee::new)
					.collect(Collectors.toCollection(ArrayList::new));

			body.put(EmployeeJSONKeys.ID, id);
			Employee newEmployee = new Employee(body);
			newEmployeeList.add(newEmployee);

			System.out.println("Employee list after deletion: " + current);
			saveNewestUser(newEmployeeList);
			return true;
		} else {
			System.out.println("The employee with id [ " + id + " ] does not exist");
			return false;
		}
	}
}
