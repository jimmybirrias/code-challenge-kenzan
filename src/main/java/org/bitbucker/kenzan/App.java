package org.bitbucker.kenzan;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;

public class App {
	//File to load the employ externally
	public static final String jsonFile = App.class.getResource("/users.json").getFile();
	public static final String BASE_URI = "http://localhost:8080/code-challenge/api/";
	
	public static HttpServer startServer () {
		 final ResourceConfig rc = new ResourceConfig().packages("org.bitbucker.kenzan");
		 return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
	}
	
	public static void main(String[] args) {
		final HttpServer server = startServer();
		try {
			server.start();
			System.out.println(String.format("Jersey app started with WADL available at " + BASE_URI));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
