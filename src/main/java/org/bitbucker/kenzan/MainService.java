package org.bitbucker.kenzan;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONObject;

import org.bitbucker.kenzan.services.EmployeeServices;
import org.bitbucket.kenza.auth.AuthorizationUtils;
import org.bitbucket.kenzan.entities.Employee;

@Path("/users")
public class MainService {
	/* Create a single instance based on singleton pattern
	 * This single instance will perform all the operations on the Employee resource */
	public EmployeeServices employeeServiceInstance = EmployeeServices.getEmployeeServiceIntance();
		
	@GET
	@Path("/allusers")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllUsers() {
		return Response.status(200).entity(employeeServiceInstance.findAllEmployees()).build();
	}
	
	@POST
	@Path("/user")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createUser (String body) {
		JSONObject jsonBody = new JSONObject(body);
		boolean status = employeeServiceInstance.createUser(jsonBody);
		JSONObject res = new JSONObject();
		res.put("status", status);
		return Response.status(status == true?201:400).entity(res.toString()).build();
	}
	
	@GET
	@Path("/user/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserById(@PathParam("id") String id) {
		System.out.println("Searching employee with the id ====> " + id);
		Employee e = employeeServiceInstance.getEmployeeByID(id);
		return Response.status(e != null?200:404).entity(e).build();
	}
	
	@PUT
	@Path("/user/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateUserById(@PathParam("id") String id, String body) {
		boolean status = employeeServiceInstance.updateUserById(id, new JSONObject(body));
		JSONObject res = new JSONObject();
		res.put("status", status);
		return Response.status(status == true?201:400).entity(res.toString()).build();
	}
	
	@DELETE
	@Path("/user/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteUserBydId(@PathParam("id") String id, @HeaderParam("token") String token) {
		JSONObject res = new JSONObject();
		
		if(token != null) {
			if(AuthorizationUtils.validateToken(token)) {
				boolean status = employeeServiceInstance.deleteUser(id);
				res.put("status", status);
				return Response.status(status == true?200:400).entity(res.toString()).build();
			}
		}
		res.put("status", false);
		return Response.status(403).entity(res.toString()).build();
	}
	

}
