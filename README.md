# Code challenge

## How clone the repository

git clone https://jimmybirrias@bitbucket.org/jimmybirrias/code-challenge-kenzan.git

## How run it
```sh
$ cd code-challenge-kenza
$ mvn clean
$ mvn install
$ mvn exec:java -Dexec.mainClass="org.bitbucker.kenzan.App"
```

## How use the api
```sh
$ curl -X GET -H 'Content-Type: application/json' -H 'Accept: application/json' -i http://localhost:8080/code-challenge/api/users/allusers
$ curl -X GET -H 'Content-Type: application/json' -H 'Accept: application/json' -i http://localhost:8080/code-challenge/api/users/{employee_id}
$ curl -X POST -H 'Content-Type: application/json' -H 'Accept: application/json' -i http://localhost:8080/code-challenge/api/users/user --data '{
    "firstName": "Jaime Test",
    "middleName": "Villasenor2",
    "lastName": "Barragan",
    "dateBirth": "1991-11-21",
    "status": "active"
}'
$ curl -X DELETE -H 'Content-Type: application/json' -H 'Accept: application/json' -H 'token: jaime123' -i http://localhost:8080/code-challenge/api/users/user/177125352
$ curl -X PUT -H 'Content-Type: application/json' -H 'Accept: application/json' -H 'token: jaime123' -i http://localhost:8080/code-challenge/api/users/user/1477288786 --data '{
    "firstName": "Jaime Updated",
    "middleName": "Villasenor",
    "lastName": "Barragan",
    "dateBirth": "1991-11-21",
    "status": "active"
}'
```